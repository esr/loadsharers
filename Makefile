.SUFFIXES: .html .adoc

.adoc.html:
	asciidoc $<

SOURCES = $(shell ls *.adoc)

all: $(SOURCES:.adoc=.html) loadsharers-logo.png

loadsharers-logo.png: loadsharers-logo.svg
	convert loadsharers-logo.svg loadsharers-logo.png

clean:
	rm -f *.html loadsharers-logo.png

upload: all
	cd ..; upload loadsharers/*.html loadsharers/*.svg loadsharers/*.png

